#!/usr/bin/python

import sys
import os

for filename in sorted(os.listdir(".")):
	if not filename.startswith("model7Mom-batch-mean"):
		continue
	lines = []
	for val in open(filename, "r").readlines():
		try:		
			lines.append(float(val))
		except:
			pass	
	print("%d: %f" % (len(lines), sum(lines) / len(lines)))
